#pragma once
#include "Scheduler.h"

class ShortestTimeToCompletion : public Scheduler
{
public:
	ShortestTimeToCompletion(std::vector<Job>);
	bool isComplete();
	Job run(int);
};