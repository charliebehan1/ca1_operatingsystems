#pragma once
#include <iostream>
#include <string>

class Job
{
public:
	std::string NAME;
	float ARRIVAL_TIME;
	float DURATION;

	Job();
	Job(std::string, float, float);
	void printDetails();
};