#include "ShortestJobFirst.h"

ShortestJobFirst::ShortestJobFirst(std::vector<Job> jobs)
	: Scheduler(jobs)
{

}

bool ShortestJobFirst::isComplete()
{
	if (jobsArrivalShortest.empty() && jobsQueued.empty())
		return true;

	return false;
}

Job ShortestJobFirst::run(int currentTick)
{
	Job currentJob;
	Job none;

	if (!jobsArrivalShortest.empty())
	{
		for (std::size_t i = 0; i < jobsArrivalShortest.size(); i++) {
			if (currentTick == jobsArrivalShortest[0].ARRIVAL_TIME)
			{
				//std::cout << "  * SJF." << jobsArrivalShortest[0].NAME << " ARRIVED";
				jobsQueued.push_back(jobsArrivalShortest[0]);
				jobsArrivalShortest.erase(jobsArrivalShortest.begin());
			}
		}
	}

	if (!jobsQueued.empty())
	{
		jobsQueued[0].DURATION--;
		if (jobsQueued[0].DURATION == 0)
		{
			std::cout << "  * SJF." << jobsQueued[0].NAME << " FINISHED";
			currentJob = jobsQueued[0];
			jobsQueued.erase(jobsQueued.begin());
		}
		else
		{
			currentJob = jobsQueued[0];
		}
		
	}
	else
	{
		currentJob = none;
	}
	return currentJob;
}
