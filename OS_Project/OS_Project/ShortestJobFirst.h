#pragma once
#include "Scheduler.h"

class ShortestJobFirst : public Scheduler
{
public:
	ShortestJobFirst(std::vector<Job>);
	bool isComplete();
	Job run(int);
};