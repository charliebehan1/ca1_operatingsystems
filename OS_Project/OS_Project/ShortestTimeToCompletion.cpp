#include "ShortestTimeToCompletion.h"

ShortestTimeToCompletion::ShortestTimeToCompletion(std::vector<Job> jobs)
	: Scheduler(jobs)
{

}

bool ShortestTimeToCompletion::isComplete()
{
	if (jobsArrival.empty() && jobsQueued.empty())
		return true;

	return false;
}

Job ShortestTimeToCompletion::run(int currentTick)
{
	Job currentJob;
	Job none;

	if (!jobsArrival.empty())
	{
		for (std::size_t i = 0; i < jobsArrival.size(); i++) {
			if (currentTick == jobsArrival[0].ARRIVAL_TIME)
			{
				//std::cout << "  * SJF." << jobsArrival[0].NAME << " ARRIVED";
				jobsQueued.push_back(jobsArrival[0]);
				jobsArrival.erase(jobsArrival.begin());
			}
		}
	}

	if (!jobsQueued.empty())
	{
		int x=0;
		for (std::size_t i = 0; i < jobsQueued.size(); i++) {
			if (jobsQueued[i].DURATION < jobsQueued[x].DURATION)
			{
				x = i;
			}
		}
		jobsQueued[x].DURATION--;
		if (jobsQueued[x].DURATION == 0)
		{
			std::cout << "  * STCF." << jobsQueued[x].NAME << " FINISHED";
			currentJob = jobsQueued[x];
			jobsQueued.erase(jobsQueued.begin()+x);
		}
		else
		{
			currentJob = jobsQueued[x];
		}
	}
	else
	{
		currentJob = none;
	}
	return currentJob;
}
