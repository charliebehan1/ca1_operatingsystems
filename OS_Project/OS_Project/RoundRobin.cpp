#include "RoundRobin.h"

RoundRobin::RoundRobin(std::vector<Job> jobs, int i)
	: Scheduler(jobs)
{
	interval = i;
}

bool RoundRobin::isComplete()
{
	if (jobsArrival.empty() && jobsQueued.empty())
		return true;

	return false;
}
int counter = 0;

Job RoundRobin::run(int currentTick)
{
	Job currentJob;
	Job none;
	int x = 0;

	if (!jobsArrival.empty())
	{
		for (std::size_t i = 0; i < jobsArrival.size(); i++) {
			if (currentTick == jobsArrival[0].ARRIVAL_TIME)
			{
				jobsQueued.push_back(jobsArrival[0]);
				jobsArrival.erase(jobsArrival.begin());
			}
		}
	}

	if (!jobsQueued.empty())
	{
		for (std::size_t i = 0; i < jobsQueued.size(); i++) {
			if (jobsQueued[i].ARRIVAL_TIME < jobsQueued[x].ARRIVAL_TIME)
			{
				x = i;
			}
		}
		jobsQueued[x].DURATION--;
		if (jobsQueued[x].DURATION == 0)
		{
			std::cout << "  * RR." << jobsQueued[x].NAME << " FINISHED";
			currentJob = jobsQueued[x];
			jobsQueued.erase(jobsQueued.begin() + x);
			counter = 0;
			return currentJob;
		}
		else
		{
			currentJob = jobsQueued[x];
		}
	}
	else
	{
		currentJob = none;
	}

	if (counter < interval)
	{
		counter++;
	}
	else
	{
		counter = 0;
		if(jobsQueued.size()>0)
			jobsQueued[x].ARRIVAL_TIME = currentTick;
	}
	return currentJob;
}
