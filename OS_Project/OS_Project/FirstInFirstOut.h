#pragma once
#include "Scheduler.h"

class FirstInFirstOut : public Scheduler
{
public:
	FirstInFirstOut(std::vector<Job>); 
	bool isComplete();
	Job run(int);
};