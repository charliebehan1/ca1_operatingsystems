#pragma once
#include "Scheduler.h"

class RoundRobin : public Scheduler
{
public:
	int interval;

	RoundRobin(std::vector<Job>, int);
	bool isComplete();
	Job run(int);
};