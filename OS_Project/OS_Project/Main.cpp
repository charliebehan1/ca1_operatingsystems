#include "Scheduler.h"
#include "FirstInFirstOut.h"
#include "ShortestJobFirst.h"
#include "ShortestTimeToCompletion.h"
#include "RoundRobin.h"
#include <fstream>

std::vector<Job> createJobs();
void printJobs(std::vector<Job> Jobs);
void tick(FirstInFirstOut FIFO, ShortestJobFirst SJF, ShortestTimeToCompletion STCF, RoundRobin RR1, RoundRobin RR2);

int main()
{
	std::vector<Job> Jobs = createJobs();

	FirstInFirstOut FIFO(Jobs);
	ShortestJobFirst SJF(Jobs);
	ShortestTimeToCompletion STCF(Jobs);
	RoundRobin RR1(Jobs, 3);
	RoundRobin RR2(Jobs, 5);

	tick(FIFO, SJF, STCF, RR1, RR2);

	std::cin.get();
}

void tick(FirstInFirstOut FIFO, ShortestJobFirst SJF, ShortestTimeToCompletion STCF, RoundRobin RR1, RoundRobin RR2)
{
	int tick = 0;
	Job currentJobFIFO, currentJobSJF, currentJobSTCF, currentJobRR1, currentJobRR2;

	std::cout << "T\tFIFO\tSJF\tSTCF\tRR1\tRR2" << std::endl;
	while (!FIFO.isComplete() && !SJF.isComplete() && !STCF.isComplete() && !RR1.isComplete() && !RR2.isComplete())
	{
		std::cout << tick << ":\t";
		std::cout << currentJobFIFO.NAME << "\t" << currentJobSJF.NAME << "\t" << currentJobSTCF.NAME << "\t" << currentJobRR1.NAME << "\t" << currentJobRR2.NAME << "\t";
		currentJobFIFO = FIFO.run(tick);
		currentJobSJF = SJF.run(tick);
		currentJobSTCF = STCF.run(tick);
		currentJobRR1 = RR1.run(tick);
		currentJobRR2 = RR2.run(tick);
		
		std::cout << std::endl;
		tick++;
	}
	std::cout << "  = SIMULATION COMPLETE" << std::endl;
}

std::vector<Job> createJobs()
{
	std::vector<Job> jobs;
	int jobCount = 0;

	std::ifstream infile("Jobs.txt");
	std::string NAME;
	int ARRIVAL_TIME, DURATION;

	while (infile >> NAME >> ARRIVAL_TIME >> DURATION)
	{
		Job job;

		job.NAME = NAME;
		job.ARRIVAL_TIME = ARRIVAL_TIME;
		job.DURATION = DURATION;

		jobs.push_back(job);

		jobCount++;
	}

	return jobs;
}

void printJobs(std::vector<Job> Jobs)
{
	for (int i = 0; i<Jobs.size(); ++i)
	{
		std::cout << "Job " << i + 1 << std::endl;
		Jobs[i].printDetails();
		std::cout << std::endl;
	}
}
