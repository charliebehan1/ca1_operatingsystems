#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Job.h"
#include <algorithm>

class Scheduler
{
public:
	std::vector<Job> jobsLoaded;
	std::vector<Job> jobsQueued;
	std::vector<Job> jobsShortest;
	std::vector<Job> jobsArrival;
	std::vector<Job> jobsArrivalShortest;
	
	Scheduler(std::vector<Job>);

	virtual Job run(int);
};