#include "Job.h"

Job::Job() {
	NAME = "N/A";
	ARRIVAL_TIME = 0;
	DURATION = 0;
}

Job::Job(std::string a, float b, float c) {
	NAME = a;
	ARRIVAL_TIME = b;
	DURATION = c;
}


void Job::printDetails()
{
	std::cout << "Name: " << NAME << std::endl;
	std::cout << "Arrival: " << ARRIVAL_TIME << std::endl;
	std::cout << "Duration: " << DURATION << std::endl;
}