#include "FirstInFirstOut.h"

FirstInFirstOut::FirstInFirstOut(std::vector<Job> jobs)
	: Scheduler(jobs)
{
}

bool FirstInFirstOut::isComplete()
{
	if (jobsArrival.empty() && jobsQueued.empty())
		return true;

	return false;
}

Job FirstInFirstOut::run(int currentTick)
{
	Job currentJob;
	Job none;

	if (!jobsArrival.empty())
	{
		for (std::size_t i = 0; i < jobsArrival.size(); i++) {
			if (currentTick == jobsArrival[0].ARRIVAL_TIME)
			{
				std::cout << "  * " << jobsArrival[0].NAME << " ARRIVED";
				jobsQueued.push_back(jobsArrival[0]);
				jobsArrival.erase(jobsArrival.begin());
			}
		}
	}

	if (!jobsQueued.empty())
	{
		jobsQueued[0].DURATION--;
		if (jobsQueued[0].DURATION == 0)
		{
			std::cout << "  * FIFO." << jobsQueued[0].NAME << " FINISHED";
			currentJob = jobsQueued[0];
			jobsQueued.erase(jobsQueued.begin());
		}
		else
		{
			currentJob = jobsQueued[0];
		}
	}
	else
	{
		currentJob = none;
	}
	return currentJob;
}
